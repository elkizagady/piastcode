<!DOCTYPE html>
<html lang="pl">
<head>
  <title>Żelkowe wygibasy</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="https://github.com/1000hz/bootstrap-validator/blob/master/js/validator.js"></script>
  <script src="skryptyStrony.js"></script>
  <link rel="stylesheet" type="text/css" href="stylStrony.css">
  <style>
  </style>
</head>
<body onload="mojaData()">


<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand"><img id="logo" src="Images/Logov3.png"/></a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="main.php">Home</a></li>
      <li><a href="ustawienia.php">Ustawienia</a></li>
      <li><a href="employers.php">Lista pracodawców</a></li>
      <li><a href="#">Page 3</a></li>
	  <li><a href="preindex.php">Wyloguj</a></li>
    </ul>
  </div>
</nav>

<div class="row">
	<div class="col-sm-2 text-left"> 
	</div>
    <div class="col-sm-8 text-left content" id="Logowanie"> 
		<div class="container">
			<h2 class="col-sm-offset-3">Informacje o użytkowniku</h2>
			<form class="form-horizontal" role="form" >
  
			<div class="col-sm-2">
			<img id="photo" src="Images/unknown.jpg" class="img-circle">
			</div>
			<div class="col-sm-7">
				<div id="label">
				<p><label>Imię: </label></p>
				<p><label>Nazwisko: </label></p>
				<p><label>Email: <?php echo $_POST["email"];?></label></p>
				<p><label>Województwo: </label></p>
				<p><label>Miasto: </label></p>
				<p><label>Branża: </label></p>
				</div>
			</div>
			
			</form>
			<form class="form-horizontal" role="form" >
				<div class="col-sm-9">
				<div class="panel-group">
					<div class="panel gold">
					  <div class="panel-heading">Umiejętności złote</div>
					  <div class="panel-body">Zawartość</div>
					</div>

					<div class="panel green">
					  <div class="panel-heading">Bardzo dobre umiejętności</div>
					  <div class="panel-body" style="color:#000000">Zawartość</div>
					</div>

					<div class="panel light-green">
					  <div class="panel-heading">Dobre umiejętności</div>
					  <div class="panel-body">Zawartość</div>
					</div>

					<div class="panel yellow">
					  <div class="panel-heading">Średnie umiejętności</div>
					  <div class="panel-body">Zawartość</div>
					</div>

					<div class="panel pink">
					  <div class="panel-heading">Słabe umiejętności</div>
					  <div class="panel-body">Zawartość</div>
					</div>

					<div class="panel red">
					  <div class="panel-heading">Bardzo słabe umiejętności</div>
					  <div class="panel-body" style="color:#000000">Zawartość</div>
					</div>
				  </div>
				</div>
			</div>
			<div>
				
			</div>
			</form>
		</div>
	</div>
</div>

<footer id="footer" class="container-fluid text-center">
  <p id="obecnaData"></p>

</footer>


</body>
</html>