<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand"><img id="logo" src="Images/Logov3.png"/></a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php">Home</a></li>
      <li><a href="ustawienia.php">Ustawienia</a></li>
      <li><a href="employers.php">Lista pracodawców</a></li>
      <li><a href="#">Page 3</a></li>
	  <li><a href="index.php">Wyloguj</a></li>
    </ul>
  </div>
</nav>

<div class="row">
	<div class="col-sm-2 text-left">
	</div>
    <div class="col-sm-8 text-left content" id="Logowanie">
		<div class="container">
			<h2 class="col-sm-offset-3">Informacje o użytkowniku</h2>
			<form class="form-horizontal" role="form" >

			<div class="col-sm-2">
			<img id="photo" src="Images/WSPK.jpg" class="img-circle">
			</div>
			<div class="col-sm-7">
				<div id="label">
				<p><label>Imię: <?php echo $_SESSION['name'] ?></label></p>
				<p><label>Nazwisko: <?php echo $_SESSION['surname'] ?></label></p>
				<p><label>Email: <?php echo $_SESSION['email'] ?></label></p>
				<p><label>Miasto: <?php echo $_SESSION['city'] ?></label></p>
				<p><label>Branża: </label></p>

				</div>
			</div>

			</form>
			<form class="form-horizontal" role="form" >
				<div class="col-sm-9">
				<div class="panel-group">
					<div class="panel gold">
					  <div class="panel-heading">Umiejętności złote</div>
					  <div class="panel-body">Programowanie</div>
					</div>

					<div class="panel green">
					  <div class="panel-heading">Bardzo dobre umiejętności</div>
					  <div class="panel-body" style="color:#000000">Chlanie</div>
					</div>

					<div class="panel light-green">
					  <div class="panel-heading">Dobre umiejętności</div>
					  <div class="panel-body">Gotowanie</div>
					</div>

					<div class="panel yellow">
					  <div class="panel-heading">Średnie umiejętności</div>
					  <div class="panel-body">Spanie</div>
					</div>

					<div class="panel pink">
					  <div class="panel-heading">Słabe umiejętności</div>
					  <div class="panel-body">Chrapanie</div>
					</div>

					<div class="panel red">
					  <div class="panel-heading">Bardzo słabe umiejętności</div>
					  <div class="panel-body" style="color:#000000">Bieganie</div>
					</div>
				  </div>
				</div>
			</div>
			<div>

			</div>
			</form>
		</div>
	</div>
</div>
