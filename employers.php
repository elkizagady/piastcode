<!DOCTYPE html>
<html lang="pl">
<head>
  <title>Żelkowe wygibasy</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="https://github.com/1000hz/bootstrap-validator/blob/master/js/validator.js"></script>
  <script src="skryptyStrony.js"></script>
  <link rel="stylesheet" type="text/css" href="stylStrony.css">
  <style>
  </style>
</head>
<body onload="mojaData()">


<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand"><img id="logo" src="Images/Logov3.png"/></a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Main.php">Home</a></li>
      <li><a href="ustawienia.php">Ustawienia</a></li>
      <li class="active"><a href="employers.php">Lista pracodawców</a></li>
      <li><a href="#">Page 3</a></li>
	  <li><a href="preindex.php">Wyloguj</a></li>
    </ul>
  </div>
</nav>


<div class="row">
	<div class="col-sm-2 text-left"> 
	</div>
    <div class="col-sm-8 text-left content" id="Logowanie"> 
		<div class="container">
		  <h2>Pracodawcy</h2>         
		  <table class="table table-hover" style="width=300px">
			<thead>
			  <tr>
				<th>Lp.</th>
				<th>Nazwa firmy</th>
				<th>Adres</th>
				<th>NIP</th>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<td>1.</td>
				<td>nazwa</td>
				<td>adres</td>
				<td>nip</td>
			  </tr>
			</tbody>
		  </table>
		</div>
	</div>
</div>

<footer id="footer" class="container-fluid text-center">
  <p id="obecnaData"></p>

</footer>


</body>
</html>