<!DOCTYPE html>
<html lang="pl">
<head>
  <title>Żelkowe wygibasy</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="https://github.com/1000hz/bootstrap-validator/blob/master/js/validator.js"></script>
  <script src="skryptyStrony.js"></script>
  <link rel="stylesheet" type="text/css" href="stylStrony.css">
  <style>
  </style>
</head>
<body onload="mojaData()">


<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand"><img id="logo" src="Images/Logov3.png"/></a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Main.php">Home</a></li>
      <li class="active"><a href="ustawienia.php">Ustawienia</a></li>
      <li><a href="employees.php">Wybrani pracownicy</a></li>
      <li><a href="#">Page 3</a></li>
	  <li><a href="preindex.php">Wyloguj</a></li>
    </ul>
  </div>
</nav>


<div class="row">
	<div class="col-sm-2 text-left"> 
	</div>
    <div class="col-sm-8 text-left content" id="Logowanie"> 
	<div class="container">
  <h2 class="col-sm-offset-1">Uzupełnij dane</h2>
  <form class="form-horizontal" role="form" >
	<div class="form-group">
      <div class="col-sm-offset-1 col-sm-7"style>          
        <div class="dropdown">
		  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Województwo
		  <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li>Dolnośląskie</li>
			<li>Kujawsko-pomorskie</li>
			<li>Lubelskie</li>
			<li>Lubuskie</li>
			<li>Łódzkie</li>
			<li>Małopolskie</li>
			<li>Mazowieckie</li>
			<li>Opolskie</li>
			<li>Podkarpackie</li>
			<li>Podlaskie</li>
			<li>Pomorskie</li>
			<li>Śląskie</li>
			<li>Świętokrzyskie</li>
			<li>Warmińsko-mazurskie</li>
			<li>Wielkopolskie</li>
			<li>Zachodniopomorskie</li>
		  </ul>
		</div>
      </div>
    </div>
	
	<div class="form-group">
      <div class="col-sm-offset-1 col-sm-7">
        <div class="dropdown">
		  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Branża
		  <span class="caret"></span></button>
		  <ul class="dropdown-menu">
			<li>IT</li>
		  </ul>
		</div>
      </div>
    </div>
	
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-7"style>
			<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading">Umiejętności</div>
					<div class="panel-body"><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#myModal">
					  <span class="glyphicon glyphicon-plus"></span> Dodaj
					</button>
					<!-- Modal -->
					  <div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog">
						
						  <!-- Modal content-->
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							  <h4 class="modal-title">Wybierz umiejętność</h4>
							</div>
							<div class="modal-body">
							  <p>tresc</p>
							</div>
							<div class="modal-footer">
							  <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
							  <button type="button" class="btn btn-default" data-dismiss="modal">Zatwierdź</button>
							</div>
						  </div>
						  
						</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
    <div class="form-group">        
      <div class="col-sm-offset-1 col-sm-6">
        <button type="Sent" class="btn btn-default">Zatwierdź</button>
      </div>
    </div>
  </form>
</div>
</div>
</div>

<footer id="footer" class="container-fluid text-center">
  <p id="obecnaData"></p>

</footer>


</body>
</html>